FROM python:alpine

WORKDIR /app
COPY requirements.txt .
COPY urlwatch.py .

RUN pip3 install -r requirements.txt
ENTRYPOINT ["python", "urlwatch.py", "--url"]
# Introduction

This script helps you monitor a single URL with live output to the console. The script checks the URL once every 60 seconds.

```text
URL is formatted correctly. Moving on
URL returned status code. Continue to monitor...
24-03-2021-16-28 - URL: https://protenus.com - status code: 200 - time elapsed: 0.117562 seconds - status code info: https://httpstatuses.com/200
24-03-2021-16-29 - URL: https://protenus.com - status code: 200 - time elapsed: 0.374586 seconds - status code info: https://httpstatuses.com/200
```

# Getting started

## Downloading the script

Copy the repo to your local environment using git:

```bash
git clone https://gitlab.com/highwatersdev/urlwatch.git
cd urlwatch
```
## Installing the dependencies
In the current directory, run the following command to install Python dependencies:
```bash
pip install -r requirements.txt
```
## Running the URL monitor
```bash
python urlwatch.py --url https://protenus.com
```
>**Note**
>The script will create urlwatch.log file in the current directory to store script output

## Running script in Docker container
You can alternatively run the script in Docker container.
In the current directory run the following command to build Docker image from `Dockerfile`:
```bash
docker build --tag localhost:urlwatch .
```
Run the container specifying the URL as the last argument:
```bash
docker run -t --rm --name urlwatch localhost:urlwatch https://protenus.com
```
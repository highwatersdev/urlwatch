"""
This script checks the status of a URL every 60 seconds.
"""
import datetime
import time
import logging
import argparse
import re
import requests
from requests.exceptions import SSLError


# Set color codes
RED = '\033[31m'
GREEN = '\033[32m'
END = '\033[0m'



def validate_url(user_url):
    """ Return boolean
    True: URL is in correct format and returns a status code
    False: URL is malformed or fails to respond properly
    Example: URL failed to return due to invalid SSL certificate
    """
    regex = re.compile(    # https://github.com/django/django/blob/76c0b32f826469320c59709d31e2f2126dd7c505/django/core/validators.py
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE
    )
    if re.match(regex, user_url):
        print("URL is formatted correctly. Moving on")
    elif re.match(regex, user_url) is None:
        raise SystemExit("Incorrectly formatted URL. Please check your URL")
    try:
        requests.get(user_url, timeout=30)
        print(f'URL returned status code. Continue to monitor...')
        return True
    except Exception as err:
        print("URL is currently down. See the error below. Continue to monitor...")
        print(err)
        return True


def monitor_url(user_url):
    """
    Runs URL check inside while loop to continuously test the URL
    Outputs the return status code and the elapsed time upon success
    """
    if validate_url(user_url):
        while True:  # Run indefinite loop
            timing = datetime.datetime.now().strftime("%d-%m-%Y-%H-%M")
            try:
                request = requests.get(user_url)
                return_code = request.status_code
                elapsed_time = request.elapsed.total_seconds()
                status_code_info = f'https://httpstatuses.com/{return_code}'
                logging.info(f'URL: {user_url} - status code: {return_code} - '
                             f'time elapsed: {elapsed_time} seconds - status code info: {status_code_info}')
                if return_code == 200:
                    print(f'{timing} - URL: {user_url} - status code: {GREEN}{return_code}{END} - '
                          f'time elapsed: {elapsed_time} seconds - status code info: {status_code_info}')
                else:
                    print(f'{timing} - URL: {user_url} - status code: {RED}{return_code}{END} - '
                          f'time elapsed: {elapsed_time} seconds - status code info: {status_code_info}')
                time.sleep(60)
            except SSLError as ssl_err:
                logging.error(ssl_err)
                print("SSL validation failed. See error below")
                print(ssl_err)
                time.sleep(60)
                continue
            except Exception as err:
                logging.error(f'Error: {err}')
                print(f'{timing} - Unable to connect to URL. See details below')
                print(err)
                time.sleep(60)
                continue


def parse_args() -> argparse.Namespace:
    """Parse the command line arguments from the user"""

    parser = argparse.ArgumentParser(
        description="This script will check URL status every 60 seconds and print output to the console"
    )
    parser.add_argument(
        "-u", "--url", required=True, help="Input URL to monitor in the format http(s)://example.com"
                                           " or http(s)://<ip_address>"
    )
    parsed_args = parser.parse_args()

    return parsed_args


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s] [%(levelname)5s] [%(module)s:%(lineno)s] %(message)s",
        filename=f"urlwatch.log"
    )
    args = parse_args()
    url = args.url
    monitor_url(url)
